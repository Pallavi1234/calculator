package calc;

public  class FloatCalculation implements ICalculator {

    Float num;

    FloatCalculation(Float num)
    {
        this.num = num;
    }

    public Float getNum() {
        return num;
    }

    @Override
    public Object addition(ICalculator obj) {

        FloatCalculation tempObj = (FloatCalculation) obj;
        return(num + tempObj.getNum());
    }

    @Override
    public Object subtraction(ICalculator obj) {
        FloatCalculation tempObj = (FloatCalculation) obj;
        return(num - tempObj.getNum());
    }

    @Override
    public Object multiplication(ICalculator obj) {
        FloatCalculation tempObj = (FloatCalculation) obj;
        return(num * tempObj.getNum());
    }

    @Override
    public Object division(ICalculator obj) {
        FloatCalculation tempObj = (FloatCalculation) obj;

        if(tempObj.getNum() == 0)
            return "Can't divide by 0";

        return(num / tempObj.getNum());
    }
}
