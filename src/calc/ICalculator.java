package calc;

public interface ICalculator {

    Object addition(ICalculator obj);
    Object subtraction(ICalculator obj);
    Object multiplication(ICalculator obj);
    Object division(ICalculator obj);
}
