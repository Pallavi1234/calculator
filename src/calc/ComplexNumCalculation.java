package calc;

import java.nio.channels.FileLock;

public class ComplexNumCalculation implements ICalculator {

    Integer real, img;

    ComplexNumCalculation(String str)
    {
        real = Integer.parseInt(str.substring(0, str.indexOf('+')).trim());
        img = Integer.parseInt(str.substring(str.indexOf('+')+1, str.indexOf('i')).trim());
    }

    public Integer getReal() {
        return real;
    }

    public Integer getImg() {
        return img;
    }

    @Override
    public Object addition(ICalculator obj) {

        ComplexNumCalculation tempObj = (ComplexNumCalculation)obj;
        String output = Integer.toString(this.real + tempObj.getReal()) + "+"
                + Integer.toString(this.img + tempObj.getImg()) + "i";
        return output;
    }

    @Override
    public Object subtraction(ICalculator obj) {

        ComplexNumCalculation tempObj = (ComplexNumCalculation)obj;
        String output = Integer.toString(this.real - tempObj.getReal()) + "+"
                + Integer.toString(this.img - tempObj.getImg()) + "i";
        return output;
    }

    @Override
    public Object multiplication(ICalculator obj) {
        return null;
    }

    @Override
    public Object division(ICalculator obj) {
        return null;
    }


}

