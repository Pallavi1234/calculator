package calc;

import javax.swing.*;
import java.util.Scanner;
import java.awt.event.*;

public class RunCalculator
{
    public final JFrame jf = new JFrame("Calculator");

    public String dataTypes[] = {"Integer","Float","Complex Number"};
    String dataTypes2[] = {"Addition","Subtraction"};

    public  JComboBox cb1 = new JComboBox(dataTypes);
    JComboBox cb3 = new JComboBox(dataTypes2);


    public String dataTypes1[] = {"Addition","Subtraction","Multiplication","Division"};
    public JComboBox cb2 = new JComboBox(dataTypes1);

    JTextField tf1 = new JTextField();
    JTextField tf2 = new JTextField();
    JLabel dispres = new JLabel();


    void CreateUI()
    {
        jf.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }
        });

        cb1.setBounds(50, 30,200,20);
        jf.add(cb1);

        cb2.setBounds(50, 70,200,20);
        jf.add(cb2);

        cb3.setBounds(50, 70,200,20);
        jf.add(cb3);
        cb3.setVisible(false);

        cb1.addItemListener(new ItemListener() {
                                public void itemStateChanged(ItemEvent event) {
                                    Object selected = cb1.getSelectedItem();
                                    if (selected == "Complex Number") {
                                        cb2.setVisible(false);
                                        cb3.setVisible(true);
                                    } else {
                                        cb2.setVisible(true);
                                        cb3.setVisible(false);
                                    }
                                }
                            });


        JLabel lbl1 = new JLabel("First Number");
        lbl1.setBounds(50, 120,120,30);
        jf.add(lbl1);

        tf1.setBounds(180, 120,120,30);
        jf.add(tf1);

        JLabel lbl2 = new JLabel("Second Number");
        lbl2.setBounds(50, 160,120,30);
        jf.add(lbl2);

        tf2.setBounds(180, 160,120,30);
        jf.add(tf2);

        JLabel result = new JLabel("Result");
        result.setBounds(50, 200,120,30);
        jf.add(result);

        dispres.setBounds(180, 200,120,30);
        jf.add(dispres);

        JButton calc = new JButton("Calculate");
        calc.setBounds(120, 260, 120, 30);
        jf.add(calc);

        jf.setLayout(null);
        jf.setSize(350, 350);
        jf.setLocationRelativeTo(null);
        jf.setVisible(true);

        calc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performCalculation();
            }
        });


    }

    void performCalculation() {
        Object datatype = cb1.getSelectedItem();

        if (datatype.toString() == "Integer") {
            performIntOperations();
        }
        else if(datatype.toString() == "Float"){
            performFloatOperations();
        }
        else if(datatype.toString() == "Complex Number"){
            performComplexNumOperation();
        }
    }

    void performIntOperations(){

        Object operation = cb2.getSelectedItem();

        IntCalculation intcalc1 = new IntCalculation(Integer.parseInt(tf1.getText().trim()));
        IntCalculation intcalc2 = new IntCalculation(Integer.parseInt(tf2.getText().trim()));

            if(operation.toString() == "Addition") {
                Object res = intcalc1.addition(intcalc2);
                dispres.setText(res.toString());
            }
            else if(operation.toString() == "Subtraction"){
                Object res = intcalc1.subtraction(intcalc2);
                dispres.setText(res.toString());
            }
            else if(operation.toString() == "Multiplication"){
                Object res = intcalc1.multiplication(intcalc2);
                dispres.setText(res.toString());
            }
            else if(operation.toString() == "Division"){
                Object res = intcalc1.division(intcalc2);
                dispres.setText(res.toString());
            }

        }

    void performFloatOperations() {

        Object operation = cb2.getSelectedItem();

        FloatCalculation fltcalc1 = new FloatCalculation(Float.parseFloat(tf1.getText().trim()));
        FloatCalculation fltcalc2 = new FloatCalculation(Float.parseFloat(tf2.getText().trim()));

        if(operation.toString() == "Addition") {
            Object res = fltcalc1.addition(fltcalc2);
            dispres.setText(res.toString());
        }
        else if(operation.toString() == "Subtraction"){
            Object res = fltcalc1.subtraction(fltcalc2);
            dispres.setText(res.toString());
        }
        else if(operation.toString() == "Multiplication"){
            Object res = fltcalc1.multiplication(fltcalc2);
            dispres.setText(res.toString());
        }
        else if(operation.toString() == "Division"){
            Object res = fltcalc1.division(fltcalc2);
            dispres.setText(res.toString());
        }

    }

    void performComplexNumOperation() {

        Object operation = cb3.getSelectedItem();

        ComplexNumCalculation complexCalc1 = new ComplexNumCalculation(tf1.getText().trim());
        ComplexNumCalculation complexCalc2 = new ComplexNumCalculation(tf2.getText().trim());

        if(operation.toString() == "Addition") {
            Object res = complexCalc1.addition(complexCalc2);
            dispres.setText(res.toString());
        }
        else if(operation.toString() == "Subtraction"){
            Object res = complexCalc1.subtraction(complexCalc2);
            dispres.setText(res.toString());
        }
       }

    public static void main(String[] args) {

        RunCalculator rc = new RunCalculator();
        rc.CreateUI();
    }
}
