package calc;

public class IntCalculation implements ICalculator {

    Integer num;

    IntCalculation(Integer num)
    {
        this.num = num;
    }

    public Integer getNum() {
        return num;
    }

    @Override
    public Object addition(ICalculator obj) {

        IntCalculation tempObj = (IntCalculation)obj;
        return(num + tempObj.getNum());
    }

    @Override
    public Object subtraction(ICalculator obj) {

        IntCalculation tempObj = (IntCalculation)obj;
        return(num - tempObj.getNum());
    }

    @Override
    public Object multiplication(ICalculator obj) {
        IntCalculation tempObj = (IntCalculation)obj;
        return(num * tempObj.getNum());
    }

    @Override
    public Object division(ICalculator obj) {

        IntCalculation tempObj = (IntCalculation)obj;
        if(tempObj.getNum() == 0)
            return "Can't divide by 0";
        return(num / tempObj.getNum());
    }
}
